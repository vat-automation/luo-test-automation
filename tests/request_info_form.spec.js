let OnlineMainTests = require('../lib/online_main/online_main.tests')
let RequestInfoFormTests = require('../lib/request_info_form/request_info_form.tests')

describe('Request Info form tests =>', () => {
    OnlineMainTests.goToBaseURL()

    OnlineMainTests.testThatClickingRequestInfoOpensForm()

    RequestInfoFormTests.testThatTheFormCanBeSubmitted()

})