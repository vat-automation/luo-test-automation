let OnlineMainTests = require('../lib/online_main/online_main.tests')
let ScreenShotTests = require('../lib/screenshot_testing/screenshot_testin.tests')

describe('Main Page tests =>', () => {
    OnlineMainTests.goToBaseURL()

    OnlineMainTests.testThatRequestButtonExists()
    OnlineMainTests.testThatUGButtonHasCorrectLink()
    OnlineMainTests.testThatGRButtonHasCorrectLink()
    OnlineMainTests.testThatDRButtonHasCorrectLink()

    //ScreenShotTests.testThatPageLooksGoodOnDesktop('ONLINE_MAIN')

    OnlineMainTests.testThatClickingEstimateCostsScrollsDownToCalculator()

    //ScreenShotTests.testThatPageLooksGoodOnDesktop('ONLINE_ESTIMATE')

})