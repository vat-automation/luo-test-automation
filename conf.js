// conf.js
var reporters = require('jasmine-reporters');
const protractorImageComparison = require('protractor-image-comparison');

exports.config = {

    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
      'browserName':'chrome',
      chromeOptions: {
        args: [ "--headless", "--window-size=2400,1800" ]
      }
    },
    specs: ['./tests/*'],
      // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
      showColors: true, // Use colors in the command line report.
      defaultTimeoutInterval: 1240000000
    },
    onPrepare: function() {
      // Add a screenshot reporter and store screenshots to `/tmp/screenshots`:\
      browser.ignoreSynchronization = true;
      var junitReporter = new reporters.JUnitXmlReporter({
        savePath: 'reports/',
        consolidateAll: false
      });
      jasmine.getEnv().addReporter(junitReporter);

      //adding screenshot/image comparison tool
      browser.protractorImageComparison = new protractorImageComparison(
          {
              baselineFolder: './screenshots/baselines/',
              screenshotPath: './screenshots/actuals/'
          }
      );

    }
  }