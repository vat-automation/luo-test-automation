//import from Protractor to use IntelliSense
var protractor = require('protractor');
var element = protractor.element;
var by = protractor.by;
var browser = protractor.browser;

const baseURL = 'https://test.liberty.edu/online/';

const goToBaseURL = () => browser.get(baseURL);

const topRequestInfoButton = () => element(by.css('div.header_top_links > a.request-info'));

const clickTopRequestInfoButton = () => topRequestInfoButton().click();

const leadForm = () => element(by.id('lead'));

const getUGButtonHref = () => element(by.css('.btn.dark.request-ug')).getAttribute('href')

const getGRButtonHref = () => element(by.css('.btn.dark.request-gr')).getAttribute('href')

const getDRButtonHref = () => element(by.css('.btn.dark.request-dr')).getAttribute('href')

const clickEstimateTuition = () => element(by.css('#bannerContent > p > a.btn.green.calc_cta')).click()


module.exports = { 
        goToBaseURL, 
        topRequestInfoButton, 
        clickTopRequestInfoButton,
        leadForm,
        getUGButtonHref,
        getGRButtonHref,
        getDRButtonHref,
        clickEstimateTuition
};