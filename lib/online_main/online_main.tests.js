//Main LUO page tests
let OnlineMainPage = require('./online_main.page')

//Constants
//const UG_URL = "https://www.liberty.edu/online/degrees/bachelors/"
const UG_URL = "https://www.liberty.edu/online/degrees/someOtherURL/" //intentionally incorrect url to show a failing test
const GR_URL = "https://www.liberty.edu/online/degrees/masters/"
const DR_URL = "https://www.liberty.edu/online/degrees/doctoral/"


//Tests
const goToBaseURL = () => OnlineMainPage.goToBaseURL()

const testThatRequestButtonExists = () => 
    it('Request selector works', () => {
        expect(OnlineMainPage.topRequestInfoButton().isPresent()).toBeTruthy()
    })

const testThatClickingRequestInfoOpensForm = () => 
    it('Clicking Request Info opens the form', () => {
        OnlineMainPage.clickTopRequestInfoButton()
        browser.sleep(5000)
        expect(OnlineMainPage.leadForm().isDisplayed()).toBeTruthy()
    })

const testThatUGButtonHasCorrectLink = () => 
    it('UG button has the correct link', () => {
        expect(OnlineMainPage.getUGButtonHref()).toEqual(UG_URL)
    })

const testThatGRButtonHasCorrectLink = () => 
    it('GR button has the correct link', () => {
        expect(OnlineMainPage.getGRButtonHref()).toEqual(GR_URL)
    })

const testThatDRButtonHasCorrectLink = () => 
    it('DR button has the correct link', () => {
        expect(OnlineMainPage.getDRButtonHref()).toEqual(DR_URL)
    })

const testThatClickingEstimateCostsScrollsDownToCalculator = () => 
    it('Clicking Estimate the cost... scrolls to the calculator', () => {
        OnlineMainPage.clickEstimateTuition()
        browser.sleep(2000)
    })


module.exports = { 
        goToBaseURL,
        testThatRequestButtonExists, 
        testThatClickingRequestInfoOpensForm,
        testThatUGButtonHasCorrectLink,
        testThatGRButtonHasCorrectLink,
        testThatDRButtonHasCorrectLink,
        testThatClickingEstimateCostsScrollsDownToCalculator
 };