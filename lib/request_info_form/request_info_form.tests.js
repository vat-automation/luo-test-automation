let RequestInfoForm = require('./request_info_form.page')

const testThatTheFormCanBeSubmitted = () => 
    it('Fills out the form and submits', () => {
        details = {
            degree: 'ACCT-AA-D',
            fName: 'tester',
            lName: 'testee',
            email: 'test@liberty.edu',
            phone: '1235551234',
            country: 'US',
            street: '123 test ave',
            zip: '12345',
            city: 'testville',
            state: 'VA'
        }
        
        RequestInfoForm.completeForm(details)
    })

module.exports = {
    testThatTheFormCanBeSubmitted
}