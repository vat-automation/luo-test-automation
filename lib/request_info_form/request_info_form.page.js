//import from Protractor to use IntelliSense
var protractor = require('protractor');
var element = protractor.element;
var by = protractor.by;
var browser = protractor.browser;

//form elements
const clickDegreeSelector = () => element(by.id('degree-study')).click();

const selectOption = (option) => 
        clickDegreeSelector().then(() => 
                element(by.css(`[value="${option}"]`)).click());


const enterFirstName = (fName) => element(by.css('#lead [name="firstname"]')).sendKeys(fName);

const enterLastName = (lName) => element(by.css('#lead [name="lastname"]')).sendKeys(lName);

const enterEmail = (email) => element(by.css('#lead [name="email"]')).sendKeys(email);

const enterPhone = (phone) => element(by.css('#lead [name="cellphone"]')).sendKeys(phone);

const clickCountrySelector = () => element(by.id('country')).click()

const selectCountry = (country) => 
        clickCountrySelector().then(() => 
                element(by.css(`[value="${country}"]`)).click());

const enterStreet = (street) => element(by.id('address1')).sendKeys(street)

const enterZip = (zip) => element(by.id('zip')).sendKeys(zip)

const enterCity = (city) => element(by.id('city')).sendKeys(city)

const enterState = (state) => element(by.id('state')).sendKeys(state)

const switchToFullAddress = () => element(by.id('NoAddress')).click();

const submit = () => element(by.id('homepageSubmit')).click()

//Complete the form and submit
const completeForm = async (details) => {
        await selectOption(details.degree)

        await enterFirstName(details.fName)
        await enterLastName(details.lName)
        await enterEmail(details.email)
        await enterPhone(details.phone)
        
        await switchToFullAddress()
        await selectCountry(details.country)
        await enterStreet(details.street)
        await enterZip(details.zip)
        await enterCity(details.city)
        await enterState(details.state)

        await submit()
}

module.exports = {
    completeForm
}