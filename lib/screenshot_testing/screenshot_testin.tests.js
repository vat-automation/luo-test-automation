//import from Protractor to use IntelliSense
var protractor = require('protractor');
var browser = protractor.browser;

const testThatPageLooksGoodOnDesktop = (pageName) => 
it(`Compares screenshots of ${pageName} on desktop`, () => {
    browser.driver.manage().window().setSize(1024, 768);
    expect(browser.protractorImageComparison.checkScreen(pageName)).toEqual(0);
    browser.driver.manage().window().setSize(2400, 1800);
})

module.exports = { testThatPageLooksGoodOnDesktop }